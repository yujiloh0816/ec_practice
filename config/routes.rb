Rails.application.routes.draw do
  root to: redirect('/public/items')

  devise_for :admins, path: 'admin', only: [:session], controllers: {
    sessions: 'admin/sessions',
  }
  devise_for :end_users, path: 'public', only: %i[session registrations], controllers: {
    sessions: 'public/sessions',
    registrations: 'public/registrations'
  }

  namespace :public do
    get 'mypage', to: 'end_users#show'
    patch 'mypage', to: 'end_users#update'
    put 'mypage', to: 'end_users#update'
    delete 'mypage', to: 'end_users#destroy'
    get 'mypage/edit', to: 'end_users#edit'
    get 'mypage/cancel', to: 'end_users#cancel'

    resources :items, only: %i[index show] do
      collection do
        get :search
      end
    end
    resources :cart_items, only: %i[index create update destroy] do
      collection do
        delete :destroy_all
      end
    end
    resources :orders, only: %i[index show new create] do
      collection do
        get :confirm
        get :thanks
      end
    end

  end

  namespace :admin do
    resources :items do
      collection do
        get :search
        get :lists
      end
    end
    resources :end_users, only: %i[index show edit update]
    resources :orders, only: %i[index show update]
    resources :order_details, only: :update
  end

end
