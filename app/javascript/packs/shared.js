$(function(){
  $("#item_autocomplete").autocomplete({
    source: function(req, resp){
      $.ajax({
        url: "/admin/items/lists",
        type: "GET",
        cache: false,
        dataType: "json",
        data: {
          term: req.term
        },
        success: function(o){
          resp(o);
        },
        error: function(xhr, ts, err){
          resp(['']);
        }
      });
    }
  });
});
