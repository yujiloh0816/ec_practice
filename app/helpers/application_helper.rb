module ApplicationHelper

  def price(price, tax: false, prefix: false, suffix: false)
    price = price.add_tax if tax
    price = price.to_s(:delimited)
    "#{'¥' if prefix} #{price} #{'円' if suffix}"
  end

end
