class OrderDetail < ApplicationRecord

  # Associations
  belongs_to :order
  belongs_to :item, optional: true

  # Enums
  enum cook_status: { restricted: 0, queued: 1, started: 2, finished: 3 } do

    event :queue do
      transition restricted: :queued, unless: -> { order.unpaid? }
    end

    event :start do
      after do
        order.cook
      end

      transition queued: :started
    end

    event :finish do
      after do
        order.prepare
      end

      transition started: :finished
    end

  end

  class << self

    def buy(cart_items)
      result = []
      cart_items.each do |ci|
        result << create!(
          item_id: ci.item_id,
          item_name: ci.name,
          tax_included_price: ci.tax_excluded_price.add_tax,
          quantity: ci.quantity
        )
      end
      cart_items.destroy_all
      result
    end

  end

  def subtotal_amount
    tax_included_price * quantity
  end

end
