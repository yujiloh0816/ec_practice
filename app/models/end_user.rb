class EndUser < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :validatable

  acts_as_paranoid

  attribute :postcode, :string
  attribute :street_address, :string

  # Callbacks
  after_find :init_default_address
  before_validation :build_initial_address, if: :new_record?
  before_validation :assign_default_address, if: :persisted?

  # Associations
  has_one :default_address, class_name: 'Address', foreign_key: 'default_end_user_id'
  has_many :addresses
  has_many :other_addresses, -> { where(default_end_user_id: nil) }, class_name: 'Address'
  has_many :cart_items, dependent: :destroy
  has_many :orders

  # Validations
  validates :last_name, presence: true
  validates :first_name, presence: true
  validates :last_name_kana, presence: true
  validates :first_name_kana, presence: true
  validates :tel, presence: true
  validates :email, presence: true, uniqueness: { conditions: -> { with_deleted } }
  validates :postcode, presence: true
  validates :street_address, presence: true

  def full_name(separator = ' ')
    [last_name, first_name].join(separator)
  end

  def full_name_kana(separator = ' ')
    [last_name_kana, first_name_kana].join(separator)
  end

  private

  def init_default_address
    self.postcode ||= default_address&.postcode
    self.street_address ||= default_address&.street_address
  end

  def assign_default_address
    default_address.assign_attributes(address_attributes)
    addresses << default_address
  end

  def build_initial_address
    build_default_address(address_attributes)
    addresses << default_address
  end

  def address_attributes
    {
      postcode: postcode,
      street_address: street_address,
      addressee: full_name
    }
  end

end
