class Order < ApplicationRecord

  # Associations
  belongs_to :end_user, -> { with_deleted }, optional: true
  has_many :order_details, dependent: :destroy

  # Alias
  alias_attribute :details, :order_details
  alias_attribute :addressee, :shipping_addressee
  alias_attribute :postcode, :shipping_postcode
  alias_attribute :street_address, :shipping_address

  # Enums
  enum payment_method: { credit: 0, bank: 1 }, _suffix: true
  enum status: { unpaid: 0, confirmed: 1, cooked: 2, prepared: 3, shipped: 4 } do

    event :pay do
      after do
        details.each(&:queue)
      end

      transition unpaid: :confirmed
    end

    event :cook do
      transition confirmed: :cooked
    end

    event :prepare do
      transition cooked: :prepared, if: -> { details.all?(&:finished?) }
    end

    event :ship do
      transition prepared: :shipped
    end

  end

  # Validates
  validates :shipping_addressee, presence: true
  validates :shipping_postcode, presence: true
  validates :shipping_address, presence: true
  validates :payment_method, presence: true

  # Delegates
  delegate :full_name, to: :end_user, prefix: true

  class << self

    def confirm(params)
      case params[:address_type]
      when 'default'
        address = Address.find(params[:default_address_id])
      when 'registered'
        address = Address.find(params[:registered_address_id])
      when 'new'
        address = nil
      end

      if address.present?
        params.merge!(address.attributes.slice('postcode', 'street_address', 'addressee'))
      end
      new(params.except('address_type', 'default_address_id', 'registered_address_id'))
    end

    def buy(params)
      ActiveRecord::Base.transaction do
        order = create!(params)
        order.details.buy(order.end_user.cart_items)
      end
    end

  end

  def items_total_amount
    details.map(&:subtotal_amount).sum
  end

  def charge
    items_total_amount + shipping_fee
  end

  def quantity
    details.pluck(:quantity).sum
  end

  def address_info
    [postcode, street_address, addressee].join(' ')
  end

end
