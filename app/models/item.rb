class Item < ApplicationRecord

  # Validations
  validates :name, presence: true, uniqueness: true
  validates :description, presence: true
  validates :tax_excluded_price, presence: true, numericality: { only_integer: true }
  validates :image, presence: true

  # Associations
  belongs_to :genre
  has_many :cart_items, dependent: :destroy
  has_many :order_details
  has_one_attached :image

  # Delegates
  delegate :name, to: :genre, prefix: true

  class << self

    def search(query)
      where('name like ?', "%#{query}%")
    end

  end

end
