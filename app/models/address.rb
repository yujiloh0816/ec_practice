class Address < ApplicationRecord

  # Callbacks
  before_destroy :not_allowed_destroy!, if: :default_end_user_present?

  # Associations
  belongs_to :end_user
  belongs_to :default_end_user, class_name: 'EndUser', foreign_key: 'default_user_id', optional: true

  # Validations
  validates :postcode, presence: true
  validates :street_address, presence: true
  validates :addressee, presence: true

  def address_info
    [postcode, street_address, addressee].join(' ')
  end

  private

  def not_allowed_destroy!
    raise 'default address not allowed destroy if exits end_user'
  end

  def default_end_user_present?
    EndUser.with_deleted.exists?(default_end_user_id)
  end

end
