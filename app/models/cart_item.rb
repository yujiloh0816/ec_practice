class CartItem < ApplicationRecord

  # Associations
  belongs_to :end_user
  belongs_to :item

  # Delegates
  delegate :name, :tax_excluded_price, :image, to: :item

  class << self

    def add!(params)
      cart_item = find_or_initialize_by(item_id: params[:item_id])
      if cart_item.new_record?
        cart_item.assign_attributes(quantity: params[:quantity])
      else
        cart_item.assign_attributes(quantity: (cart_item.quantity + params[:quantity].to_i))
      end
      cart_item.save!
    end

  end

  def subtotal_amount
    tax_excluded_price * quantity
  end

end
