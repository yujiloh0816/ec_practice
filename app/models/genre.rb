class Genre < ApplicationRecord

  # Associations
  has_many :items

  # Scopes
  scope :only_active, -> { where(is_active: true) }

end
