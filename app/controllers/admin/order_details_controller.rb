class Admin::OrderDetailsController < Admin::BaseController

  def update
    order_detail = OrderDetail.find(params[:id])
    result = case params[:order_detail][:cook_status]
             when 'started'
               order_detail.start
             when 'finished'
               order_detail.finish
             else
               false
             end

    if result
      redirect_to admin_order_path(order_detail.order), notice: '更新しました'
    else
      msg = "#{order_detail.cook_status_i18n}から#{OrderDetail.cook_statuses_i18n[params[:order_detail][:cook_status]]}に変更できません"
      redirect_to admin_order_path(order_detail.order), alert: msg
    end
  end

end
