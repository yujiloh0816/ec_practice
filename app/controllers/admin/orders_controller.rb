class Admin::OrdersController < Admin::BaseController

  def index
    @orders = Order.includes(:end_user, :order_details).all
  end

  def show
    @order = Order.find(params[:id])
  end

  def update
    @order = Order.find(params[:id])
    result = case params[:order][:status]
             when 'confirmed'
               @order.pay
             when 'shipped'
               @order.ship
             end
    if result
      redirect_to admin_order_path(@order), notice: '更新しました'
    else
      msg = "#{@order.status_i18n}から#{Order.statuses_i18n[params[:order][:status]]}に変更できません"
      redirect_to admin_order_path(@order), alert: msg
    end
  end

end
