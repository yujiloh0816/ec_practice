class Admin::ItemsController < Admin::BaseController
  def index
    @items = Item.includes(:genre).all
  end

  def search
    @genres = Genre.only_active
    @items = Item.includes(:genre).search(params[:query])
    render 'public/items/index'
  end

  def new
    @item = Item.new
  end

  def create
    @item = Item.new(item_params)
    if @item.save
      redirect_to admin_items_path, notice: '商品を新規作成しました'
    else
      flash.now[:alert] = '商品の作成に失敗しました'
      render :new
    end
  end

  def show; end

  def edit; end

  def update; end

  def lists
    @item_names = Item.order(:name).where("name like ?", "%#{params[:term]}%").pluck(:name)
    render json: @item_names
  end

  private

  def item_params
    params.require(:item).permit(
        :genre_id,
      :is_active,
      :name,
      :description,
      :tax_excluded_price,
      :image
    )
  end

end
