class Public::CartItemsController < Public::BaseController

  def index
    @cart_items = CartItem.includes(:item).where(end_user_id: current_end_user.id)
  end

  def create
    current_end_user.cart_items.add!(cart_item_params)
    redirect_to public_cart_items_path, notice: 'カードに追加しました'
  end

  def update
    cart_item = CartItem.find(params[:id])
    cart_item.update!(cart_item_params)
    redirect_to public_cart_items_path, notice: 'カート内の商品を更新しました'
  end

  def destroy
    cart_item = CartItem.find(params[:id])
    cart_item.destroy
    redirect_to public_cart_items_path, notice: 'カート内の商品を削除しました'
  end

  def destroy_all
    cart_items = CartItem.where(end_user_id: current_end_user.id)
    cart_items.destroy_all
    redirect_to public_cart_items_path, notice: 'カートを空にしました'
  end

  private

  def cart_item_params
    params.require(:cart_item).permit(:item_id, :quantity)
  end

end
