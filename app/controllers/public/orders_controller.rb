class Public::OrdersController < Public::BaseController

  def index; end

  def show; end

  def new
    if current_end_user.cart_items.present?
      @order = Order.new
    else
      redirect_to public_cart_items_path, alert: '商品を追加してください'
    end
  end

  def confirm
    @cart_items = current_end_user.cart_items
    @order = Order.confirm(order_confirm_params)
    if @order.valid?
      render :confirm
    else
      render :new
    end
  end

  def create
    Order.buy(order_params)
    redirect_to thanks_public_orders_path
  end

  def thanks; end

  private

  def order_params
    params.require(:order).permit(
      :end_user_id,
      :shipping_addressee,
      :shipping_postcode,
      :shipping_address,
      :payment_method
    )
  end

  def order_confirm_params
    params.require(:order_confirm).permit(
      :end_user_id,
      :address_type,
      :default_address_id,
      :registered_address_id,
      :shipping_addressee,
      :shipping_postcode,
      :shipping_address,
      :payment_method
    )
  end
  
end
