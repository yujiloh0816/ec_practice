class Public::ItemsController < Public::BaseController

  def top; end

  def index
    @items = Item.all
    @genres = Genre.only_active

    if params[:genre_id]
      @genre = Genre.find(params[:genre_id])
      @items = @items.where(genre_id: params[:genre_id])
    end
  end

  def search
    @genres = Genre.only_active
    @items = Item.includes(:genre).search(params[:query])
    render :index
  end

  def show
    @genres = Genre.only_active
    @item = Item.find(params[:id])
  end

end
