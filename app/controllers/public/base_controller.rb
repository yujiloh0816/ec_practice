class Public::BaseController < ApplicationController
  before_action :authenticate_end_user!, unless: :item_controller?

  layout 'public'

  private

  def item_controller?
    params[:controller] == 'public/items'
  end

end

