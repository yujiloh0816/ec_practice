# frozen_string_literal: true

class Public::RegistrationsController < Devise::RegistrationsController
  layout 'public'

  before_action :configure_sign_up_params, only: [:create]

  protected

  def configure_sign_up_params
    devise_parameter_sanitizer.permit(:sign_up, keys: %i[last_name first_name last_name_kana first_name_kana tel email password postcode street_address])
  end

  # The path used after sign up.
  def after_sign_in_path_for(resource)
    public_mypage_path
  end

end
