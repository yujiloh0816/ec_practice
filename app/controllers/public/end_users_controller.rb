class Public::EndUsersController < Public::BaseController

  def show
    @end_user = EndUser.find(current_end_user.id)
  end

  def edit
    @end_user = EndUser.find(current_end_user.id)
  end

  def update
    @end_user = EndUser.find(current_end_user.id)
    if @end_user.update(end_user_params)
      redirect_to public_mypage_path, notice: '更新しました'
    else
      render :edit
    end
  end

  def cancel; end

  def destroy
    current_end_user.destroy
    redirect_to new_end_user_registration_path, notice: '退会しました'
  end

  private

  def end_user_params
    params.require(:end_user).permit(
      :last_name,
      :first_name,
      :last_name_kana,
      :first_name_kana,
      :email,
      :postcode,
      :street_address,
      :tel
    )
  end

end
