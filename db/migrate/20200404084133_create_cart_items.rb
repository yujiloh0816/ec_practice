class CreateCartItems < ActiveRecord::Migration[6.0]
  def change
    create_table :cart_items do |t|
      t.belongs_to :end_user
      t.belongs_to :item
      t.integer :quantity, null: false

      t.timestamps
    end
  end
end
