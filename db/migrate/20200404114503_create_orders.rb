class CreateOrders < ActiveRecord::Migration[6.0]
  def change
    create_table :orders do |t|
      t.belongs_to :end_user
      t.string :shipping_addressee, null: false
      t.string :shipping_postcode, null: false
      t.string :shipping_address, null: false
      t.integer :payment_method, null: false
      t.integer :status, null: false, default: 0
      t.integer :shipping_fee, null: false, default: 800

      t.timestamps
    end
  end
end
