class CreateOrderDetails < ActiveRecord::Migration[6.0]
  def change
    create_table :order_details do |t|
      t.belongs_to :order
      t.belongs_to :item
      t.string :item_name, null: false
      t.integer :cook_status, null: false, default: 0
      t.integer :tax_included_price, null: false
      t.integer :quantity, null: false

      t.timestamps
    end
  end
end
