class CreateAddresses < ActiveRecord::Migration[6.0]
  def change
    create_table :addresses do |t|
      t.belongs_to :end_user
      t.integer :default_end_user_id
      t.string :postcode, null: false
      t.string :street_address, null: false
      t.string :addressee, null: false

      t.timestamps
    end

    add_index :addresses, :default_end_user_id
  end
end
