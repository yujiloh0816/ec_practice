class CreateItems < ActiveRecord::Migration[6.0]
  def change
    create_table :items do |t|
      t.belongs_to :genre
      t.boolean :is_active, null: false, default: true
      t.string :name, null: false
      t.text :description, null: false
      t.integer :tax_excluded_price, null: false

      t.timestamps
    end

    add_index :items, :name, unique: true
  end
end
