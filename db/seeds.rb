# 管理者を作成
admin = Admin.create!(email: "admin#{rand(1..100)}@admin.com", password: 'admin123')

# ジャンルと商品を作成
genres_and_items = {
  ケーキ: %w[チーズケーキ ショートケーキ],
  プリン: %w[カスタードプリン チョコレートプリン],
  焼き菓子: [],
  キャンディ: []
}
genres_and_items.each do |genre, items|
  genre = Genre.create!(name: genre, is_active: true)
  items.each do |item|
    i = genre.items.build(
      name: item,
      description: "#{item}の説明",
      tax_excluded_price: [100, 200, 300].sample
    )
    i.image.attach(
      io: File.open(Rails.root.join('public', 'sample.jpeg')),
      filename: 'sample.jpeg',
      content_type: 'img/jpeg'
    )
    i.save!
  end
end

puts '================'
puts "admin:  email: #{admin.email}, password: admin123"
puts '================'