class Integer

  def add_tax
    (self * 1.08).round(0)
  end

end
